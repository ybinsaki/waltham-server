[Unit]
Description=waltham server
Before=network.target
Wants=network.target

[Service]
ExecStart=@CMAKE_INSTALL_PREFIX@/bin/wthp-server
ExecStop=/usr/bin/killall -s KILL wthp-server
Restart=always
RestartSec=5

[Install]
WantedBy=multi-user.target
