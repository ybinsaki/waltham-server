# wthp gst server
[![](https://img.shields.io/badge/License-MIT-blue.svg?style=flat-square)]()

Overview

## Table of Contents
  * [Usage](#usage)
  * [Requirements](#requirements)
  * [Installation](#installation)

## Usage

```bash
# default port 34400
$ wthp-server

# support help
$ wthp-server -h
```

## Requirements
  - waltham
  - cmake
  - Linux

## Installation

```bash
$ mkdir build && cd build
$ cmake && make
```
